<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->
<!-- Start responsesArea  -->

<?php if ( have_comments() ) : ?>
	<div class="responsesArea">
	<h2 id="comments"><?php comments_number('No Comments', '1 Comment', '% Comments' );?> on &#8220;<?php the_title(); ?>&#8221;</h2>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

	<ul class="commentlist">
	<?php wp_list_comments('style=li&callback=lap_comments'); ?>
	</ul>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
	</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments">Comments are closed.</p>
	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>

<div class="contactArea" id="respond">
	<h2>Comment Form</h2>
	
	<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
	<p>You must be <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">logged in</a> to post a comment.</p>
	<?php else : ?>

	<!-- start of comment form-->
		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" class="formArea">
			<?php comment_id_fields(); ?>

				<?php if ( $user_ID ) : ?>

				<p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="Log out of this account">Log out &raquo;</a></p>

				<?php else : ?>

				<p><input type="text" name="author" />Your name</p>
				<p><input type="text" name="email" />Your email</p>
				<p><input type="text" name="url" />Your URL</p>

				<?php endif; ?>
				<p><textarea name="comment" id="comment" rows="12" cols="12"></textarea></p>
				<p><input type="submit" class="post" value="" /></p>

			<?php do_action('comment_form', $post->ID); ?>
		</form>
	<!-- end of comment form-->

	<?php endif; // If registration required and not logged in ?>
</div>
<?php endif; // if you delete this the sky will fall on your head ?>