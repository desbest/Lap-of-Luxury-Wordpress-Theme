<?php

add_theme_support('post-thumbnails');
add_theme_support( 'automatic-feed-links' );

function lapofluxury_widgets_init(){
	register_sidebar(array(
		'id' 			=> "left-sidebar",
		'name'			=> 'Left Sidebar',
	    'before_widget' => '<div id="%1$s" class="widget %2$s">',
	    'after_widget' => '</div>',
	    'before_title' => '<h3>',
	    'after_title' => '</h3>',
	));

	 register_sidebar(array(
	 	'id' 			=> "right-sidebar",
		'name'			=> 'Right Sidebar',
	    'before_widget' => '<div id="%1$s" class="widget %2$s">',
	    'after_widget' => '</div>',
	    'before_title' => '<h3>',
	    'after_title' => '</h3>',
	));
}
add_action('widgets_init', 'lapofluxury_widgets_init');

function register_my_menu() {
register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function lap_comments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment; 

	?>
	<li id="li-comment-<?php comment_ID() ?>">
		<?php echo get_avatar($comment,$size='64',$default='<path_to_url>' ); ?>
		<h3><?php printf(__('%s'), get_comment_author_link()) ?>
			<a class="date" href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a>
		</h3>
		<div class="comment-content" id="comment-<?php comment_ID(); ?>">
			<?php edit_comment_link(__('(Edit)'),'  ','') ?>
			 <?php if ($comment->comment_approved == '0') : ?>
				 <em><?php _e('Your comment is awaiting moderation.') ?></em>
				 <br />
			  <?php endif; ?>
			  <?php comment_text() ?>

			   <div class="reply">
				 <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
			  </div>
		</div>  
	<?php
}


function dp_recent_comments($no_comments = 10, $comment_len = 100,$post_id=0) { 
    global $wpdb; 
	
	$post_id = (int) $post_id;
		
	$request = "SELECT * FROM $wpdb->comments";
	$request .= " JOIN $wpdb->posts ON ID = comment_post_ID";
	$request .= " WHERE comment_approved = '1' AND post_status = 'publish' AND post_password =''"; 

	if ( $post_id > 0 )
	{
		$request .= " AND comment_post_ID='$post_id'";
	}
	$request .= " ORDER BY comment_date DESC LIMIT $no_comments"; 
		
	$comments = $wpdb->get_results($request);
		
	if ($comments) { 
		foreach ($comments as $comment) { 
			ob_start();
			?>
				<li>
					<a href="<?php echo get_permalink( $comment->comment_post_ID ) . '#comment-' . $comment->comment_ID; ?>"><?php echo $comment->post_title;?></a>
				</li>
			<?php
			ob_end_flush();
		} 
	} else { 
		echo "<li>No comments</li>";
	}
}

function truncate($text, $limit = 25, $ending = '...') {
	if (strlen($text) > $limit) {
		$text = strip_tags($text);
		$text = substr($text, 0, $limit);
		$text = substr($text, 0, -(strlen(strrchr($text, ' '))));
		$text = $text . $ending;
	}
	
	return $text;
}

?>