<?php get_header(); ?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<!-- Start contentArea  -->
<div class="contentArea">
	<h2>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
	</h2>
	<div class="entry-content">
		<?php the_content('Read the rest of this entry &raquo;'); ?>
		<?php wp_link_pages(); ?>
		<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
</div>
<!-- End contentArea  -->

<?php endwhile; ?>
<?php include("nav.php"); ?>
<?php else : ?>

<?php include("404.php"); ?>
<?php endif; ?>

<?php get_footer(); ?>