<!-- Start list  -->
<div class="list">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('left-sidebar')) { ?>
	<div class="widget">
	<h3><img src="<?php bloginfo('template_url')?>/images/categories.gif" alt="Categories" /></h3>
	<ul>
		<?php wp_list_categories('depth=1')?>
	</ul>
	</div>
<?php } ?>
</div>
<!-- End list  -->
<div class="list2">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('right-sidebar')) { ?>
<div class="widget">
<h3><img src="<?php bloginfo('template_url')?>/images/archives.gif" alt="Archives" /></h3>
<ul>
	<?php wp_get_archives(); ?>
</ul>
</div>
<div class="widget">
<h3><img src="<?php bloginfo('template_url')?>/images/links.gif" alt="Links" /></h3>
<ul>
<?php wp_list_bookmarks('title_li=&categorize=0') ?>
</ul>
</div>
<?php } ?>
</div>