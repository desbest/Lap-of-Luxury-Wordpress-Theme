<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php $theTitle=wp_title(" - ", false); if($theTitle != "") { ?><title><?php echo wp_title("",false); ?> - <?php bloginfo('name'); ?></title>
<?php } else { ?><title><?php bloginfo('name'); ?></title><?php } ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
<!-- outdated in newer versions of wordpress  -->
<!-- <link rel="alternate" type="application/rss+xml" title="<?php //bloginfo('name'); ?> RSS Feed" href="<?php //bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php //bloginfo('name'); ?> RSS Feed" href="<?php //bloginfo('atom_url'); ?>" /> -->
<!-- <link rel="pingback" href="<?php //bloginfo('pingback_url'); ?>" /> -->
<?php wp_head(); ?>
<meta name="viewport" content="width=device-width,initial-scale=1" />
</head>
<body>
<!-- Start wrapper  -->
<div id="wrapper">
	<!-- Start header  -->
	<div id="header">
		<span class="rss">
			<a href="<?php bloginfo('rss_url'); ?>" title="Subscribe to RSS Feed">Subscribe to RSS Feed</a>
		</span>
		<!-- Start search  -->
		<span class="search">
			<form method="get" action="<?php echo get_option('home'); ?>">
			<b>keyword</b><input type="text" name="s"/><input type="submit" class="submit" value="" title="Search" />
			</form>
		</span>
		<!-- End search  -->
		<!-- Site Logo -->
		<a href="<?php echo get_option('home'); ?>/" title="Lap of Luxury" class="logo"><?php bloginfo('title')?></a>
		<!-- Start navigation  -->
		<a class="hide" href="#skip" accesskey ="j" >Press "j" for Skip Navigation</a>
		<!-- <ul class="nav">
			<li <?php //if(is_home()) echo 'class="current_page_item"'; ?>><a href="<?php //echo get_option('home'); ?>/">Home</a></li>
			<?php
			//wp_list_pages('title_li=&sort_column=menu_order&depth=1');
			?>
		</ul> -->
		<?php wp_nav_menu( array( 'theme_location' => 'main-menu', 'menu_class' => 'nav' ) ); ?>
		<span id="skip"></span>				
		<!-- End navigation  -->
	</div>
	<!-- End header  -->
	<!-- Start mainBody  -->
	<div id="mainBody">
		<div class="mainArea">
		<!-- Start main  -->
		<div class="main">